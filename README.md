# VSCode DevContainer with PlantUML

Use PlantUML extension for VSCode inside a DevContainer.

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

## Architecture

The PlantUML diagram below serves as an example and an overview of how this example works.


```plantuml
@startuml

rectangle Host {
    [VSCode]
}

rectangle DevContainer {
    rectangle Docker-in-Docker {
        [PlantUMLServer]
    }
}

VSCode -> PlantUMLServer : 8080
@enduml
```

## How it was built

Create a Docker-in-Docker devcontainer and add the following configuration to devcontainer.json (do not delete or replace existing configuration, only add).

```json
{
	"settings": {
		"plantuml.render": "PlantUMLServer",
		"plantuml.server": "http://localhost:8080/"
	},
	"extensions": [
		"jebbs.plantuml"
	],
	"forwardPorts": [
        8080
    ],
	"postCreateCommand": "docker run -d -p 8080:8080 plantuml/plantuml-server:jetty",
}
```
Copyright © 2022 Stoney Jackson and Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
